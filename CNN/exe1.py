import tensorflow as tf
from tensorflow.keras import layers

x = tf.random.normal([1, 7, 7, 4])
layer = layers.Conv2D(4, kernel_size=5, strides=1, padding='valid')
layer = layers.Conv2D(4, kernel_size=5, strides=2, padding='same')
pool = layers.MaxPool2D(2, strides=2)
layer = layers.UpSampling2D(size=3)
relu = tf.nn.relu(x)
print(relu.shape)