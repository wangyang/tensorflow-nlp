Fait-il chaud dehors?
La température est élevée en été
Comment consommer moins d'essence?
Il faut rouler plus lentement
Comment moins polluer?
Ne pas avoir de voiture Diesel
Est-ce que je peux manger des fraises en hiver?
Non, car c’est meilleur pour l’environnement de manger des fruits et légumes de saison
Que permet le passage à une alimentation biologique?
De réduire les impacts négatifs sur la santé
Est-ce bien de manger des protéines animales?
Non, ça a un fort impact sur l’environnement
Comment remédier au gaspillage alimentaire?
anticiper sur les commandes et les achats
Le plastique est-t'il bon pour la planete?
Ce matériau a des effets négatifs sur l’environnement et sur la santé
Qu’est-ce que le compost ?
Un phénomène naturel de décomposition des végétaux
Dans quels déchets trouve-t-on des produits polluants ou des métaux lourds ?
Les batteries et piles
Combien de litres d'eau représent, en moyenne, un bain ?
220 litres
Combien faut-il de litres d'eau pour fabriquer un kilo de papier ?
500 litres
Quel facteur consomme le plus d'énergie durant le cycle d'une machine à laver le linge ?
La température choisie
Quels sont les sacs les moins polluants ?
Les sacs plastiques réutilisables
Comment luter contre la pollution?
Planter des arbres, mais pas n'importe lesquels
A quoi servent les panneaux solaires?
Ils créer de l'électricité tout le temps
Les sacs réutilisables sont ils une bonne alternative?
Oui et Non, ils poluent presque autant que les sac normaux
Comment traiter le verre?
Il faut le recycler
Comment dois-je envoyer mon courrier?
Par mail ou par lettre, la pollution sera équivalent
Que doit-on des restes de produits dangereux après usage ?
On les rapporte à la déchetterie
Le vélo en libre-service est-t'il une solution?
Le velo-service permet de proposer de nombreux vélo, qui sont écologique
Que veut dire la montée du niveau des mers et des océans?
Le réchauffement climatique qui fait fondre le niveau de la mer
Dois-je prendre de l'eau au robinet ou en bouteille?
Les bouteilles polluent et sont plus cher que l'eau du robinet
Les français produissent plus de'ordures ménagères?
La quantité d'ordures ménagères à doublé en 40 ans
Quelle est l'empreinte carbonne de la France?
En 2020, 552 millions de tonnes de CO2
Comment réduire son empreinte carbonne en voiture?
Réduire sa vitesse de 10 km/h
Quelles sont les solutions pour recycler?
Réutiliser les écorces de clémentine et de citron
Comment se déplacer en limitant son impact carbonne?
Utiliser son vélo
Que faire pour moi polluer à la maison?
Gérer sa consommation d'eau
Qu'est ce qui consomme le plus dans ma maison?
Le lave-linge représente 25% de la consommation énergétique d'un foyer
Comment sauver la planète?
Changer ses habitudes de consommation
Que représente l'élevage dans l'émission de gaz à effet de serre?
L'élevage représente 15% des émissions
Où doit-t'on jeter les piles?
On peut les jeter dans des poubelles spéciales
Pourquoi ne doit-t'on pas jeter les piles à la poubelle?
Car elle relachent des métaux qui polluent l'environnement en se dégradant
Quel objet plastique pouvons-nous remplacer?
Les bouteilles plastiques ne sont pas toutes recyclès
Comment sera l'avenir de la planète?
Il est sombre si on ne fait rien et si on ne change pas nos habitudes de vie
Lequel de ces pays émet le plus de gaz à effet de serre ?
Les États-Unis
Quel pays a le plus d'émissions de gaz à effet de serre PAR HABITANT ?
L'Australie
Quel est le secteur le plus énergivore en France ?
Le secteur du Bâtiment
En matière de transport, quelle action aurait le plus d’impact ?
Améliorer l’efficience des avions
Quel gaz a l'effet de serre le plus puissant ?
La vapeur d'eau
Où va la majorité du CO2 émis par l’Homme ?
Dans l'atmosphère
En fondant, quels types de glace entraînent une montée des eaux ?
Les glaciers de montagne
Quelle est la conséquence du réchauffement de l'océan?
Il absorbera moins de CO2
Quelle a été l’élévation du niveau des mers depuis 1900 ?
Augmentation de 10 à 20 cm
Quelle est le meuilleur ordinateur ?
Choisissez un ordinateur portable plutôt qu'un ordinateur de bureau ,ça peut économiser jusqu’à 80% moins d’énergie.
Bonjour?
Bonjour!