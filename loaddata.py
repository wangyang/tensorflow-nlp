import tensorflow as tf
from tensorflow.keras import optimizers
from keras import datasets, optimizers

(x, y), (x_test, y_test) = datasets.mnist.load_data()
print(x.shape)
train_db = tf.data.Dataset.from_tensor_slices((x, y))
# 打乱顺序，移动最大距离10000
train_db.shuffle(10000)
# 输入一个预处理函数，根据结果生成新的处理好的数据集
# train_db.map(functionPreprocess)
# 将数据分组，之后按组访问
train_db.batch(32)
# 对于该数据，for语句执行的重复次数，repea（）死循环
train_db.repeat(32)
