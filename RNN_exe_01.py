import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

cell = layers.SimpleRNNCell(3)
cell.build(input_shape=(None, 4))
print(cell.trainable_variables)

x = tf.random.normal([4, 80, 100])
xt0 = x[:, 0, :]  # 第一个单词
cell_p = layers.SimpleRNNCell(64)
out, ht1 = cell_p(xt0, [tf.zeros([4, 64])])
print(out.shape, id(out))
print(ht1[0].shape, id(ht1[0]))
