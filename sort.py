import random

import numpy as np
import tensorflow as tf
from keras import layers
from keras import datasets, optimizers
from tensorflow import keras

a = tf.random.shuffle(tf.range(5))
print(a)
print(tf.sort(a, direction="DESCENDING"))
print(tf.sort(a, direction="ASCENDING"))
idx = tf.argsort(a, direction="DESCENDING")
b = tf.sort(a, direction="DESCENDING")
print(a)
print(idx)
print(tf.gather(a, idx))
# top_k
c = tf.random.uniform([3, 3], maxval=10, dtype=tf.int32)
print(c)
res = tf.math.top_k(c, 2)
print(res.indices)
print(res.values)


# top_k 准确度，前n个中存在正确预测，则认为预测准确
# out为神经网路的输出结果，b个数据，N个分类概率，target为b个数据的实际分类，topk为一维向量，是topk取值序列
def accuracy(out, target, topk=(1,)):
    maxk = max(topk)
    # batch size 为target数据个数
    batch_size = target.shape[0]

    # 对于每个数据，前maxk个可能性的位置
    pred = tf.math.top_k(out, maxk).indices
    # 转至预言结果，每个maxk取值，第几可能性，都有对应的预测结果
    pred = tf.transpose(pred, perm=[1, 0])
    # 将target扩充
    target_ = tf.broadcast_to(target, pred.shape)
    # 比较预测和目标
    correct = tf.equal(pred, target_)

    res = []
    for k in topk:
        # 前k行，维度变换为一维，并将boolean型转化为float型，前k个可能，最多能命中1个
        correct_k = tf.cast(tf.reshape(correct[:k], [-1]), dtype=tf.float32)
        # 求和
        correct_k = tf.reduce_sum(correct_k)
        # 计算精确度
        acc = float(correct_k / batch_size)
        res.append(acc)
    return res
