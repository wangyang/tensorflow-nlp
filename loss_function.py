import tensorflow as tf

# MES,(sum(y-out)^2)/N=>N=(batch*v.lenth)
y = tf.constant([0, 1, 3, 2, 0])
y = tf.one_hot(y, depth=4)
y = tf.cast(y, dtype=tf.float32)

out = tf.random.normal([5, 4])
loss1 = tf.reduce_mean(tf.square(y - out))
loss2 = tf.square(tf.norm(y - out)) / (5 * 4)
loss3 = tf.reduce_mean(tf.losses.MSE(y, out))
y=tf.random.normal([6,24,24,255])
print('loss1', loss1)
print('loss2', loss3)
print('loss3', loss3)
# entropy，值越小说明给与的信息越多，分类效果越好
a = tf.fill([4], 0.25)
entropy = -tf.reduce_sum(a * tf.math.log(a) / tf.math.log(2.))
print(entropy)
# cross entropy，h（p，q）=-Pi*logQi Pi为实际分类概率，Qi为预测分类概率
crossE = tf.losses.categorical_crossentropy([0, 1, 0, 0], [0.25, 0.25, 0.25, 0.25])
print(crossE)

x = tf.random.normal([1, 784])
w = tf.random.normal([784, 2])
b = tf.zeros([2])
logits = x @ w + b
prob = tf.math.softmax(logits, axis=1)
logits=tf.reshape(logits,[2])
print(tf.losses.categorical_crossentropy([0, 1], logits, from_logits=True))
#print(tf.losses.categorical_crossentropy([0, 1], prob))
