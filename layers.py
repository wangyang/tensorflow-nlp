
import tensorflow as tf


x = tf.random.normal([4, 784])

net = tf.keras.layers.Dense(512)
net.build(input_shape=(None, 784))
out = net(x)

print(out.shape)
print(net.kernel.shape, net.bias.shape)


