import random


class State:
    def __init__(self):
        self.value = 0
        self.t = 0

    def action(action):
        return None


class State1(State):
    def __init__(self):
        self.value = 0
        self.t = 0

    def action(self, action):
        if action == 1:
            next = State2()
            self.value = max(next.value + 0, self.value)
        elif action == 4:
            next = State3()
            self.value = max(next.value + 0, self.value)
        else:
            next = State1()
            self.value = max(next.value - 1 * 0.9 ** self.t, self.value)
        self.t = self.t + 1
        return next


class State2(State):
    def __init__(self):
        self.value = 0
        self.t = 0

    def action(self, action):
        if action == 2:
            next = State1()
            self.value = max(next.value + 0, self.value)
        elif action == 4:
            next = State4()
            self.value = max(next.value - 0.5 * 0.9 ** self.t, self.value)
        else:
            next = State2()
            self.value = max(next.value - 1 * 0.9 ** self.t, self.value)
        self.t = self.t + 1
        return next


class State3(State):
    def __init__(self):
        self.value = 0
        self.t = 0

    def action(self, action):
        if action == 1:
            next = State4()
            self.value = max(next.value + 0, self.value)
        elif action == 3:
            next = State1()
            self.value = max(next.value + 0, self.value)
        else:
            next = State3()
            self.value = max(next.value - 1 * 0.9 ** self.t, self.value)
        self.t = self.t + 1
        return next


class State4(State):
    def __init__(self):
        self.value = 0
        self.t = 0

    def action(self, action):
        next = State1()
        self.value = max(next.value + 1 * 0.9 ** self.t, self.value)
        self.t = self.t + 1
        return next


def train():
    s1 = State1()
    s2 = State2()
    s3 = State3()
    s4 = State4()
    States = (s1, s2, s3, s4)

    for t in range(50):
        for s in States:
            n = random.randint(1, 4)
            s = s.action(n)

    for s in States:
        print(s.value)


train()
