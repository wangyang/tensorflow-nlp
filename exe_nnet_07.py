import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import datasets, layers, optimizers, Sequential, metrics


def preprocess(x, y):
    x = tf.cast(x, dtype=tf.float32) / 255.
    x = tf.reshape(x, [28 * 28])
    y = tf.cast(y, dtype=tf.int32)
    y = tf.one_hot(y, depth=10)
    return x, y



(x, y), (x_val, y_val) = datasets.mnist.load_data()
batchsz = 128
train_db = tf.data.Dataset.from_tensor_slices((x, y))
train_db = train_db.map(preprocess).shuffle(10000).batch(batchsz)

test_db = tf.data.Dataset.from_tensor_slices((x_val, y_val))
test_db = test_db.map(preprocess).batch(batchsz)


class MyDense(layers.Layer):

    def __init__(self, inp_dim, outp_dim):
        super(MyDense, self).__init__()

        self.kernel = self.add_weight('w', [inp_dim, outp_dim])
        self.bias = self.add_weight('b', [outp_dim])

    def call(self, inputs, traning=None):
        out = inputs @ self.kernel + self.bias

        return out


class MyModel(keras.Model):
    def __init__(self):
        super(MyModel, self).__init__()
        self.fc1 = MyDense(28 * 28, 256)
        self.fc2 = MyDense(256, 128)
        self.fc3 = MyDense(128, 64)
        self.fc4 = MyDense(64, 32)
        self.fc5 = MyDense(32, 10)

    def call(self, inputs, traning=None):
        x = self.fc1(inputs)
        x = tf.nn.relu(x)
        x = self.fc2(x)
        x = tf.nn.relu(x)
        x = self.fc3(x)
        x = tf.nn.relu(x)
        x = self.fc4(x)
        x = tf.nn.relu(x)
        x = self.fc5(x)

        return x


network = MyModel()
network.build(input_shape=[None, 784])
network.summary()

network.compile(optimizer=optimizers.Adam(learning_rate=0.01),
                loss=tf.losses.CategoricalCrossentropy(from_logits=True),
                metrics=['accuracy']
                )
network.fit(train_db, epochs=5, validation_data=test_db, validation_freq=2)

network.evaluate(test_db)

sample = next(iter(test_db))
x = sample[0]
pred = network.predict(x)

loaded_model = tf.keras.models.load_model('network')
print('load_model')
loaded_model.evaluate(test_db)
# print('save weights')
# network2 = Sequential([
#     layers.Dense(256, activation=tf.nn.relu),
#     layers.Dense(128, activation=tf.nn.relu),
#     layers.Dense(64, activation=tf.nn.relu),
#     layers.Dense(32, activation=tf.nn.relu),
#     layers.Dense(10)
# ])
# network2.compile(optimizer=optimizers.Adam(learning_rate=0.01),
#                  loss=tf.losses.CategoricalCrossentropy(from_logits=True),
#                  metrics=['accuracy']
#                  )
# network2.load_weights('weights.ckpt')
# print('load weights')
# network2.evaluate(test_db)
# network.save("model.h5")
# network=tf.keras.models.load_model("model.h5")

