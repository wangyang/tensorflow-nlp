from pprint import pprint

from nltk.corpus import stopwords
import string
from nltk.tokenize import word_tokenize
import re
import nltk
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
# Lecture du fichier
file = open('theguardian.txt', 'r')

read_file = file.read()

# Ici le nettoyage du texte(plus exactement la préparation aux traitements qui
# vont suivre) consiste à le mettre en minuscule puis supprimer
# les signes de ponctuation et les chiffres.

text = read_file.lower()
# pprint(text)
new_txt = re.sub(r"[0-9,-\.\"\'\“]+", '', text)
# pprint(new_txt)
# Tokenisation

words = word_tokenize(new_txt)

# 文档词库
nltk.download('stopwords')
# the a an in 等不必要词汇

stop_words = set(stopwords.words('english'))

# 各种符号
pct = list(string.punctuation)

# Tokenisation

words1 = word_tokenize(text)

new_sentence = []
for word in words1:
    if ((word not in stop_words) & (word not in pct)):
        new_sentence.append(word)
# print(new_sentence)
print("tage\n")
# 词性标注
nltk.download('averaged_perceptron_tagger')

list_tags = pos_tag(new_sentence)

print(list_tags)
# 按照词性分类
dict_freq = {}
for e in list_tags:
    e_tag = e[1]
    if e_tag not in dict_freq.keys():
        dict_freq[e_tag] = [e[0]]
    else:
        if (e[0] not in dict_freq[e_tag]):
            dict_freq[e_tag].append(e[0])

print(dict_freq)
# 查看词性的含义
nltk.help.upenn_tagset('NN.*')
nltk.help.upenn_tagset('MD')
nltk.help.upenn_tagset('WRB')
nltk.help.upenn_tagset('VB.*')
nltk.download('wordnet')
# lemmatisation 词形还原
for numw in range(len(list_tags)):
    tag = list_tags[numw][1]
    if tag.startswith("NN"):
        pos = 'n'
    elif tag.startswith('VB'):
        pos = 'v'
    else:
        pos = 'a'
    l = WordNetLemmatizer().lemmatize(list_tags[numw][0], pos)
    print(list_tags[numw][0], l)
