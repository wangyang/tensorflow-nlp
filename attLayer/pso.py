import random
from math import *
import matplotlib.pyplot as plt


# Initialisation d'une particule

def initUne(dim, inf, sup):
    pos = [random.uniform(inf, sup) for i in range(dim)]
    val = eval(pos)
    return {'vit': [0] * dim, 'pos': pos, 'val': val, 'bestpos': pos, 'bestval': val, 'bestvois': []}


# Init de la population
def initEssaim(nb, dim, inf, sup):
    essaim = [initUne(dim, inf, sup) for i in range(nb)]
    for nump in range(nb):
        laParticule = essaim[nump]

        bestvoisin = []
        for nump1 in range(nb):
            if (nump1 != nump):
                laParticule1 = essaim[nump1]
                if (bestvoisin == []):
                    bestvoisin = laParticule1['pos']
                elif (laParticule1['val'] < eval(bestvoisin)):
                    bestvoisin = laParticule1['pos']

        laParticule['bestvois'] = bestvoisin
        essaim[nump] = laParticule
    return essaim


# Les 4 fonctions que nous considérons dans ce TP

def sphere(sol):
    d = len(sol)
    val = 0
    for i in range(d):
        val = val + sol[i] * sol[i]
    return val


def griewank(sol):
    d = len(sol)

    s = 0
    for i in range(d):
        s = s + sol[i] * sol[i]

    p = 1
    for i in range(d):
        p = p * cos(sol[i] / sqrt(i + 1))

    return s / 4000 - p + 1


def rosenbrock(sol):
    d = len(sol)
    val = 0
    for i in range(d - 1):
        val = val + 100 * (sol[i + 1] - sol[i] * sol[i]) * (sol[i + 1] - sol[i] * sol[i]) + (sol[i] - 1) * (sol[i] - 1)
    return val


def schwefel(sol):
    d = len(sol)
    val = 418.9829 * d
    for i in range(d):
        val = val - sol[i] * sin(sqrt(fabs(sol[i])))
    return val


def eval(sol):
    global FONCTION
    if FONCTION == "sphere":
        return sphere(sol)
    elif FONCTION == "griewank":
        return griewank(sol)
    elif FONCTION == "rosenbrock":
        return rosenbrock(sol)
    elif FONCTION == "schwefel":
        return schwefel(sol)
    else:
        print("Fonction inconnue!")
        exit(1)


def meilleureSolution(essaim):
    meilleure_pos, meilleure_val = essaim[0]['bestpos'], essaim[0]['bestval']

    for nump in range(1, len(essaim)):
        if (essaim[nump]['bestval'] < meilleure_val):
            meilleure_pos, meilleure_val = essaim[nump]['bestpos'], essaim[nump]['bestval']

    return meilleure_pos, meilleure_val


# Programme principal
# L'utilisateur doit choisir (1) la dimension du problème
# et (2) la fonction à minimiser.

FONCTION = ''
while (FONCTION not in ['sphere', 'griewank', 'rosenbrock', 'schwefel']):
    print(FONCTION)
    FONCTION = input('Donnez le nom de la fonction à minimiser :')

dim = 1
while (dim <= 1):
    dim = int(input('Donnez la dimension du problème :'))

print("Minimisation de la fonction ", FONCTION, ' en ', dim, ' dimensions')

if (FONCTION == 'sphere'):
    inf = -100
    sup = 100
elif (FONCTION == 'griewank'):
    inf = -600
    sup = 600
elif (FONCTION == 'rosenbrock'):
    inf = -5
    sup = 10
elif (FONCTION == 'schwefel'):
    inf = -500
    sup = 500
else:
    inf = 0
    sup = 0

# Initialisations
nbParticules = 20
psi = 0.7298844
cmax = 1.47

essaim = initEssaim(nbParticules, dim, inf, sup)

nbIterations = 2000


def pso(essaim):
    vr = 0.7
    cm = 1.42
    for num in range(1, len(essaim)):
        for i in range(dim):
            num['vit'][i] = \
                num['vit'][i] * vr + cm * random.random() * (num['bestpos'][i]
                                                             - num['pos'][i]) + vr + cm * random.random() * (
                        num['bestvois'][i] - num['pos'][i])
            num['pos'][i] = num['pos'][i] + num['vit']
            if eval(num['pos']) < num['val']:
                num['val'] = eval(num['pos'])
                num['bestpos'] = num['pos']
