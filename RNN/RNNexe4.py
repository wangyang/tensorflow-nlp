from pprint import pprint

from nltk.corpus import twitter_samples
from nltk.twitter import Twitter
import tensorflow as tf
import nltk

nltk.download("twitter_samples")

all_tweets = twitter_samples.strings("tweets.20150430-223406.json")
negative_tweets = twitter_samples.strings("negative.json")
positive_tweets = twitter_samples.strings("positive.json")

phrase = all_tweets[0]
pprint(negative_tweets[0])
sentence = nltk.word_tokenize(phrase)
pprint(sentence)