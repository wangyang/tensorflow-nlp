import tensorflow as tf
from dateutil import utils
from tensorflow import keras
from keras import layers, optimizers
import numpy as np
import tensorflow_addons as tfa


def train():
    # 我已经帮大家封装了日期生成器
    data = utils.DateData(4000)

    # 建立模型
    model = Seq2Seq(...)

    # training
    for t in range(1500):
        bx, by, decoder_len = data.sample(32)
        loss = model.step(bx, by, decoder_len)


class Seq2Seq(keras.Model):
    def __init__(self):
        super(Seq2Seq, self, units).__init__()
        self.units = units
        # encoder
        self.encode_embedding = layers.Embedding()
        self.encoder = layers.LSTM(units=units, return_sequences=True, return_state=True)
        self.decode_embedding = layers.Embedding()
        self.decode_cell = layers.LSTMCell(units=units)
        self.decode_Dense=layers.Dense(dec_v_dim)

        self.decoder_train = tfa.seq2seq.BasicDecoder(
            cell=self.decode_cell,
            sampler=tfa.seq2seq.sampler.TrainingSampler(),
            output_layer=self.decode_Dense
        )

        self.cross_entropy = keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        self.optimizer = optimizers.Adam(learning_rate=0.001)

    def encode(self, x):
        embedded = self.encode_embedding(x)
        initial_state = [tf.zeros([x.shape[0], self.units]), tf.zeros([x.shape[0], self.units])]
        o, h, c = self.encoder(embedded, initial_state)
        return [h, c]

