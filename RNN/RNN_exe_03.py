import tensorflow as tf
import numpy as np
import os
from tensorflow import keras
from tensorflow.keras import datasets, layers, optimizers, Sequential, metrics

tf.random.set_seed(22)
np.random.seed(22)
assert tf.__version__.startswith('2.')
# the most frequenst words
total_word = 10000
max_length = 80
embedding_len = 100
(x_train, y_train), (x_text, y_text) = datasets.imdb.load_data(num_words=total_word)
x_train = keras.preprocessing.sequence.pad_sequences(x_train, maxlen=max_length)
x_text = keras.preprocessing.sequence.pad_sequences(x_text, maxlen=max_length)

batchsz = 128
train_db = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_db = train_db.shuffle(10000).batch(batchsz, drop_remainder=True)

test_db = tf.data.Dataset.from_tensor_slices((x_text, y_text))
test_db = test_db.batch(batchsz, drop_remainder=True)

print('x_train.shape', x_train.shape, tf.reduce_max(y_train), tf.reduce_min(y_train))
print('x_test.shape', x_text.shape)


class MyRNN(keras.Model):
    def __init__(self, units):
        super(MyRNN, self).__init__()
        self.state0 = [tf.zeros([batchsz, units]), tf.zeros([batchsz, units])]
        self.state1 = [tf.zeros([batchsz, units]), tf.zeros([batchsz, units])]
        # transform text to embedding representation
        # [b,80]=>[b,80,100]
        self.embedding = layers.Embedding(total_word, embedding_len, input_length=max_length)
        # self.rnn_cell0 = layers.SimpleRNNCell(units, dropout=0.2)
        # self.rnn_cell1 = layers.SimpleRNNCell(units, dropout=0.2)
        self.rnn_cell0 = layers.LSTMCell(units, dropout=0.5)
        self.rnn_cell1 = layers.LSTMCell(units, dropout=0.5)

        # [b,80,100]=>[b,1]
        self.outlayer = layers.Dense(1)

    def call(self, inputs, training=None):
        """

        :param inputs:[b,80,1]
        :param training:
        :return:
        """
        # [b,80]
        x = inputs
        # embedding [b,80]=>[b,80,100]
        x = self.embedding(x)
        # rnn cell compute[b,80,100]=[b,64]
        state0 = self.state0
        state1 = self.state1
        for word in tf.unstack(x, axis=1):  # word:[b,100]
            # x*wxh+h*whh
            out0, state0 = self.rnn_cell0(word, state0, training)
            out1, state1 = self.rnn_cell1(out0, state1, training)

        # out[b,64]
        # [b,64]=>[b,1]
        x = self.outlayer(out1)
        prob = tf.sigmoid(x)
        return prob


def main():
    import time
    units = 60
    epochs = 4
    t0 = time.time()
    model = MyRNN(units)

    model.compile(optimizer=optimizers.Adam(learning_rate=0.001),
                  loss=tf.losses.BinaryCrossentropy(),
                  metrics=['accuracy'])
    model.fit(train_db, epochs=epochs, validation_data=test_db)
    model.evaluate(test_db)
    t1 = time.time()
    print('total time:', t1 - t0)


if __name__ == '__main__':
    main()
