import tensorflow as tf

af = tf.linspace(-6, 6, 10)
print(af)
bf = tf.sigmoid(af)
print('sigmod', bf)
cf = tf.nn.softmax(af)
print('softmax', cf)
df = tf.tanh(af)
print('tanh', df)
