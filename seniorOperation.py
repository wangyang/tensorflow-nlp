import tensorflow as tf

a = tf.random.normal([3, 3])
mask = a > 0
print(mask)
indices = tf.where(mask)
print(indices)
ga = tf.gather_nd(a, indices)
print(ga)

A = tf.ones([3, 3])
B = tf.zeros([3, 3])
# true 返回第一矩阵对应值，false第二矩阵
print(tf.where(mask, B, A))

indices = tf.constant([[2], [6], [8]])
update = tf.constant([1, 2, 3])
shape = tf.constant([10])
print(indices)
print(update)
print(shape)
scan = tf.scatter_nd(indices, update, shape)
print(scan)
# meshgrid
y = tf.linspace(-2, 2, 5)
x = tf.linspace(-2, 2, 5)
points_x, points_y = tf.meshgrid(x, y)
print(points_x, points_y)
points = tf.stack([points_x, points_y], axis=2)
print(points)
