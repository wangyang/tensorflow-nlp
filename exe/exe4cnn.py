from pprint import pprint

import tensorflow as tf
from matplotlib import pyplot as plt
from tensorflow import keras
from tensorflow.keras import datasets, layers, optimizers, Sequential, metrics

(x_train, y_train), (x_test, y_test) = datasets.mnist.load_data()

x_train = x_train[:1000:]

y_train = y_train[:1000:]
x_test = x_test[:100:]
y_test = y_test[:100:]
x_train = tf.reshape(x_train, [1000, 28, 28, 1])
print(x_train.shape, )
x_test = tf.expand_dims(x_test, -1)
print(x_test.shape)
x_train = tf.cast(x_train, dtype=tf.float32)
x_test = tf.cast(x_test, dtype=tf.float32)
x_train = tf.convert_to_tensor(x_train, dtype=tf.float32) / 255

x_test = tf.convert_to_tensor(x_test, dtype=tf.float32) / 255
y_train = tf.convert_to_tensor(y_train, dtype=tf.int32)
y_test = tf.convert_to_tensor(y_test, dtype=tf.int32)
y_train = tf.one_hot(y_train, depth=10)
y_test = tf.one_hot(y_test, depth=10)
print(y_train.shape)

model = Sequential([
    # 核数，核尺寸，是否填充，激活函数
    layers.Conv2D(16, kernel_size=[4, 4], padding="valid", activation=tf.nn.relu),
    layers.Conv2D(16, kernel_size=[4, 4], padding="valid", activation=tf.nn.relu),
    # 池化核，填充

    layers.MaxPool2D(pool_size=[2, 2], strides=1, padding='valid'),
    layers.Dropout(0.4),
    layers.Flatten(),
    layers.Dense(64, activation=tf.nn.relu),
    layers.Dropout(0.4),
    layers.Dense(10)
])

model.build(input_shape=[None, 28, 28, 1])

model.summary()

model.compile(optimizer='adam',
              loss=tf.losses.CategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])
train_db = (x_train, y_train)

test_db = (x_test, y_test)

history = model.fit(x_train, y_train, epochs=10, validation_data=test_db)

plt.plot(history.history['accuracy'], label='accuracy')
plt.plot(history.history['val_accuracy'], label='val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')
plt.show()

