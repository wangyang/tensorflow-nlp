import tensorflow as tf
from tensorflow import keras
import numpy as np
from tensorflow.keras import datasets, layers, optimizers, Sequential, metrics

dataset = np.loadtxt("pima-indians-diabetes.csv",
                     delimiter=",")
X = dataset[:, 0:8]
# x[𝑠𝑡𝑎𝑟𝑡: 𝑒𝑛𝑑: 𝑠𝑡𝑒𝑝]
Y = dataset[:, 8]
print(X.shape, Y.shape)
X_train = X[:600, ]
Y_train = Y[:600, ]
X_test = X[600:, ]
Y_test = Y[600:, ]

model = Sequential([
    layers.Dense(12, input_dim=8,activation=tf.nn.sigmoid),
    layers.Dense(8, activation=tf.nn.relu),
    layers.Dense(1, activation=tf.nn.sigmoid)
])
model.summary()
model.get_config()
model.compile(loss='binary_crossentropy',
              optimizer='adam', metrics=['accuracy'])
model.fit(X_train, Y_train, epochs=150, batch_size=10)
scores = model.evaluate(X_test, Y_test)
print("\n%s: %.2f%%" % (model.metrics_names[1],
                        scores[1] * 100))
