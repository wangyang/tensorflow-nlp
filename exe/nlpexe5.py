from pprint import pprint

from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer

from nltk.corpus import twitter_samples
from nltk.corpus import stopwords

import string

import random

import re

from nltk import classify
from nltk import NaiveBayesClassifier


# L'objectif de ce code est de construire un classifieur
# bayésien faisant de l'analyse de sentiments.
# Autrement dit un classifieur qui une fois l'apprentissage
# terminé, associe à chaque tweet qu'il reçoit en entrée
# la classe Positive ou Negative.


def lemmatize_sentence(tokens):
    lemmatizer = WordNetLemmatizer()
    lemmatized_sentence = []
    for word, tag in pos_tag(tokens):
        if tag.startswith('NN'):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'
        lemmatized_sentence.append(lemmatizer.lemmatize(word, pos))
    return lemmatized_sentence


# La fonction suivante supprime le bruit

def remove_noise(tweet_tokens, stop_words=()):
    cleaned_tokens = []
    for token, tag in pos_tag(tweet_tokens):
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|' \
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', token)
        token = re.sub("(@[A-Za-z0-9_]+)", "", token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)

        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    return cleaned_tokens


# Programme principal

# 1. D'abord découpage en tokens et lemmatisation des
# tokens
positive_tweet_tokens = twitter_samples.tokenized('positive_tweets.json')
negative_tweet_tokens = twitter_samples.tokenized('negative_tweets.json')

stop_words = stopwords.words('english')

for num in range(len(positive_tweet_tokens)):
    positive_tweet_tokens[num] = lemmatize_sentence(positive_tweet_tokens[num])

for num in range(len(negative_tweet_tokens)):
    negative_tweet_tokens[num] = lemmatize_sentence(negative_tweet_tokens[num])

print('***********', positive_tweet_tokens[0])

# 2. Suppression du bruit. On considère comme du bruit
# toute partie du texte qui n'ajoute rien à son sens.
# Ici, on supprimera les hyperliens
# (http://** et https://), les adresses Twitter (@xxx),
# les signes de ponctuations, les caractères spéciaux
# et les stopwords que nous avons introduits
# dans le premier TP (fot, in, with, ...). Nous appelons la fonction remove_noise()



positive_cleaned_tokens_list = []
negative_cleaned_tokens_list = []

for tokens in positive_tweet_tokens:
    positive_cleaned_tokens_list.append(remove_noise(tokens, stop_words))

for tokens in negative_tweet_tokens:
    negative_cleaned_tokens_list.append(remove_noise(tokens, stop_words))

print('***********', positive_cleaned_tokens_list[0])

