import tensorflow as tf

from keras import datasets, optimizers
from matplotlib import pyplot

(x_train, y_train), (x_test, y_test) = datasets.mnist.load_data()
#x[0,255]=>[0,1]

x = tf.convert_to_tensor(x_train, dtype=tf.float32)/255
y = tf.convert_to_tensor(y_train, dtype=tf.int32)

print(x.shape, x.dtype, y.shape, y.dtype)
print(tf.reduce_min(x), tf.reduce_max(x))
print(tf.reduce_min(y), tf.reduce_max(y))



for i in range(9):
  pyplot.subplot(330+1+i)
  pyplot.imshow(x_train[i], cmap=pyplot.get_cmap('gray'))
  pyplot.show()
