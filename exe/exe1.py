import tensorflow as tf

a = tf.zeros([3, 3])
b = tf.ones([3, 3])
print(a)
print(b)
c = tf.constant([[1., 1., 1.], [1., 1., 0.], [1., 0., 0.]])
print(c)
d = c * b
print(d)
e = tf.random.normal([28, 28, 255])
print(e.shape)
f = tf.random.normal([3, 28, 28, 255])
print(f.shape)
x = tf.random.normal([4, 784])

net = tf.keras.layers.Dense(512)
net.build(input_shape=(None, 784))
out = net(x)
print(out.shape)