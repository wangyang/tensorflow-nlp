import numpy as np
import tensorflow as tf
from tensorflow.keras import optimizers
from keras import datasets, optimizers

(x, y), _ = datasets.mnist.load_data()
x = tf.convert_to_tensor(x, dtype=tf.float32) / 50.
y = tf.convert_to_tensor(y, dtype=tf.int32)
y= tf.one_hot(y, depth=10)
print('x:', x.shape, 'y:', y)
train_db = tf.data.Dataset.from_tensor_slices((x, y)).batch(128).repeat(30)
# 将数据转换为可循环访问的，再取第一个值
x, y = next(iter(train_db))

print('sample', x.shape, y.shape)


def main():
    w1 = tf.Variable(tf.random.truncated_normal([784, 256], stddev=0.1))
    b1 = tf.Variable(tf.zeros([256]))
    w2 = tf.Variable(tf.random.truncated_normal([256, 128], stddev=0.1))
    b2 = tf.Variable(tf.zeros([128]))
    w3 = tf.Variable(tf.random.truncated_normal([128, 10], stddev=0.1))
    b3 = tf.Variable(tf.zeros([10]))

    optimizer = optimizers.gradient_descent_v2.SGD(learning_rate=0.05)
    for step, (x, y) in enumerate(train_db):
        x = tf.reshape(x, [-1, 28 * 28])
        with tf.GradientTape() as tape:  # 只跟踪tf.variable
            h1 = x @ w1 + b1
            h1 = tf.nn.relu(h1)
            h2 = h1 @ w2 + b2
            h2 = tf.nn.relu(h2)
            out = h2 @ w3 + b3

            # mse= mean(sum((y_one_hot-out)^2))
            # [b,10]

            loss = tf.square(y - out)
            loss = tf.reduce_mean(loss, axis=1)
            # mean scalar
            loss = tf.reduce_mean(loss)

        grads = tape.gradient(loss, [w1, b1, w2, b2, w3, b3])
        # print("before")
        # for g in grads:
        #     print(tf.norm(g))

        grads, total_norm = tf.clip_by_global_norm(grads, 15)

        # print("after")
        # for g in grads:
        #     print(tf.norm(g))

        optimizer.apply_gradients(zip(grads, [w1, b1, w2, b2, w3, b3]))

        if step % 100 == 0:
            print(step, 'loss :', float(loss))


# 神经网络可变参数向量集合grads，使用 clip by global norm，可以对所有grads向量求2范数和再整体缩放。范数最佳取值0-20
# new_grads,total_norm=tf.clip_by_global_norm(grads,25)
# print(total_norm)
main()
