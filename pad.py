import numpy as np
import tensorflow as tf
from keras import layers
from keras import datasets, optimizers
from tensorflow import keras

a = tf.reshape(tf.range(9), [3, 3])
b = tf.pad(a, [[1, 1], [1, 1]])
print(b)
print(tf.tile(a, [1, 2]))

# 张量限幅，限制数据的取值范围
# 小于3被替换成3
print(tf.maximum(a, 3))
# 大于6被替换成6
print(tf.minimum(a, 6))
# 限定取值范围3-6
print(tf.clip_by_value(a, 3, 6))
a = a - 3
print(tf.nn.relu(a))
# 等价于
print(tf.maximum(a, 0))
# clip by norm，用范数计算向量模长，转变成单位向量再乘以希望的模长，从而限定向量各元素的取值范围
c = tf.random.normal([2, 2], mean=10)
print(c)
print(tf.norm(c))
cc = tf.clip_by_norm(c, 15)
print(cc)
print(tf.norm(cc))
