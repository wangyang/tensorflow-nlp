import numpy as np
import tensorflow as tf
from keras import layers
from keras import datasets, optimizers
from tensorflow import keras

a = tf.ones([4, 28, 3])
b = tf.ones([2, 28, 3])
c = tf.concat([a, b], axis=0)
print(c.shape)
d = tf.ones([4, 28, 3])
e = tf.stack([a, d], axis=0)
print(e.shape)
ff, gg = tf.unstack(e, axis=0)
print(ff.shape, gg.shape)
k = tf.unstack(e, axis=3)
print(k[0].shape)
l = tf.split(e, axis=3, num_or_size_splits=[1, 2])
print(l[0].shape, l[1].shape, l[1].shape[1])
