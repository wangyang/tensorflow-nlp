import random

import numpy as np
import tensorflow as tf
from keras import layers
from keras import datasets, optimizers
from tensorflow import keras as keras
from tensorflow.keras.layers import Dense as dense

x = tf.random.normal([2, 3])
model = keras.Sequential([
    dense(2, activation='relu'),
    dense(2, activation='relu'),
    dense(2)
])


model.build(input_shape=[None, 3])
model.summary()


for p in model.trainable_variables:
    print(p.name, p.shape)
