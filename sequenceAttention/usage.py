from pprint import pprint

import nltk
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from sequenceAttention.ConversationData import ConversationData

from sequenceAttention.kerasModel import ModelTrain, ModelPredict

nltk.download('punkt')
data = ConversationData()

model = ModelTrain(
    data.num_word, data.num_word, emb_dim=80, units=64, attention_layer_size=20)
model.build(input_shape=[(None, 35), (None, 35), (None,)])

model.load_weights('weights.ckpt')

modelPredict = ModelPredict(model.encoder, model.decoder, max_pred_len=35, start_token=data.start_token,
                            end_token=data.end_token)


def chat(text):
    text = data.analyserText(text)
    pred = modelPredict(text)
    res = data.idx2str(pred[0])
    return res


def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Bonjour!')


def echo(update, context):
    """Echo the user message."""
    words = nltk.word_tokenize(update.message.text)
    if set(words) < set(data.vocab):
        update.message.reply_text(chat(update.message.text))
    else:
        res = "Désolé, je ne comprends pas les mots, " + str(set(words) - set(data.vocab))
        update.message.reply_text(res)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("2064998233:AAHo6xyX4aGN_4kU9Y-89J_xnuIvbl6qrSw", use_context=True)
    # Get the dispatcher to register handlers
    print("ready!")
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()



if __name__ == '__main__':
    main()
