import tensorflow as tf
from tensorflow import keras

import numpy as np
import utils
import tensorflow_addons as tfa
import datetime

from sequenceAttention.ConversationData import ConversationData


class Encoder(keras.layers.Layer):
    def __init__(self, enc_v_dim, emb_dim, units):
        super(Encoder, self).__init__()
        self.units = units
        self.enc_embeddings = keras.layers.Embedding(
            input_dim=enc_v_dim, output_dim=emb_dim,  # [enc_n_vocab, emb_dim]
            embeddings_initializer=tf.initializers.RandomNormal(0., 0.1),
        )
        self.encoder = keras.layers.LSTM(units=units, return_sequences=True, return_state=True)

    def call(self, inputs):
        x = inputs
        o = self.enc_embeddings(x)
        init_s = [tf.zeros((tf.shape(x)[0], self.units)), tf.zeros((tf.shape(x)[0], self.units))]
        o, h, c = self.encoder(o, initial_state=init_s)
        return o, h, c


class Decoder(keras.layers.Layer):
    def __init__(self, dec_v_dim, emb_dim, units, attention_layer_size, encoder):
        super(Decoder, self).__init__()
        self.units = units
        self.attention = tfa.seq2seq.LuongAttention(units, memory=None, memory_sequence_length=None)
        self.encoder = encoder
        self.decoder_cell = tfa.seq2seq.AttentionWrapper(
            cell=keras.layers.LSTMCell(units=units),
            attention_mechanism=self.attention,
            attention_layer_size=attention_layer_size,
            alignment_history=True,  # for attention visualization
        )
        self.dec_embeddings = keras.layers.Embedding(
            input_dim=dec_v_dim, output_dim=emb_dim,  # [dec_n_vocab, emb_dim]
            embeddings_initializer=tf.initializers.RandomNormal(0., 0.1),
        )
        self.decoder_dense = keras.layers.Dense(dec_v_dim)  # output layer
        # train decoder
        self.decoder_train = tfa.seq2seq.BasicDecoder(
            cell=self.decoder_cell,
            sampler=tfa.seq2seq.sampler.TrainingSampler(),  # sampler for train
            output_layer=self.decoder_dense
        )
        self.cross_entropy = keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        self.opt = keras.optimizers.Adam(0.05, clipnorm=5.0)

    def call(self, x, y, seq_len):
        o, h, c = self.encoder(x)
        # encoder output for attention to focus
        self.attention.setup_memory(o)
        # wrap state by attention wrapper
        state = self.decoder_cell.get_initial_state(batch_size=tf.shape(o)[0], dtype=tf.float32).clone(
            cell_state=[h, c])
        dec_in = y[:, :-1]  # ignore <EOS>
        dec_emb_in = self.dec_embeddings(dec_in)
        o, _, _ = self.decoder_train(dec_emb_in, state, sequence_length=seq_len)
        logits = o.rnn_output
        return logits


class ModelTrain(keras.Model):
    def __init__(self, enc_v_dim, dec_v_dim, emb_dim, units, attention_layer_size):
        super(ModelTrain, self).__init__()
        self.encoder = Encoder(enc_v_dim, emb_dim, units)
        self.decoder = Decoder(dec_v_dim, emb_dim, units, attention_layer_size, self.encoder)
        self.cross_entropy = keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        self.opt = keras.optimizers.Adam(0.05, clipnorm=5.0)

    def call(self, inputs):
        x, y, seq_len = inputs
        x = tf.cast(x, dtype=tf.int32)
        y = tf.cast(y, dtype=tf.int32)
        seq_len = tf.cast(seq_len, dtype=tf.int32)
        with tf.GradientTape() as tape:
            logits = self.decoder(x, y, seq_len)
            dec_out = y[:, 1:]  # ignore <GO>
            loss = self.cross_entropy(dec_out, logits)
            grads = tape.gradient(loss, self.trainable_variables)
        self.opt.apply_gradients(zip(grads, self.trainable_variables))
        return loss


class ModelPredict(keras.Model):
    def __init__(self, encoder, decoder, max_pred_len, start_token, end_token):
        super(ModelPredict, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

        self.decoder_eval = tfa.seq2seq.BasicDecoder(
            cell=decoder.decoder_cell,
            sampler=tfa.seq2seq.sampler.GreedyEmbeddingSampler(),  # sampler for predict
            output_layer=decoder.decoder_dense
        )
        self.max_pred_len = max_pred_len
        self.start_token = start_token
        self.end_token = end_token

    def call(self, x, return_align=False, training=False):
        o, h, c = self.encoder(x)
        # encoder output for attention to focus
        self.decoder.attention.setup_memory(o)
        # wrap state by attention wrapper
        state = self.decoder.decoder_cell.get_initial_state(batch_size=tf.shape(o)[0], dtype=tf.float32).clone(
            cell_state=[h, c])
        done, i, s = self.decoder_eval.initialize(
            self.decoder.dec_embeddings.variables[0],
            start_tokens=tf.fill([tf.shape(x)[0], ], self.start_token),
            end_token=self.end_token,
            initial_state=state,
        )
        pred_id = np.zeros((tf.shape(x)[0], self.max_pred_len), dtype=np.int32)
        for l in range(self.max_pred_len):
            o, s, i, done = self.decoder_eval.step(
                time=l, inputs=i, state=s, training=False)
            pred_id[:, l] = o.sample_id

        if return_align:
            return np.transpose(s.alignment_history.stack().numpy(), (1, 0, 2))
        else:
            s.alignment_history.mark_used()  # otherwise gives warning
            return pred_id


def train():
    data = ConversationData()
    model = ModelTrain(
        data.num_word, data.num_word, emb_dim=80, units=64, attention_layer_size=20)

    model.build(input_shape=[(None, 20), (None, 20), (None,)])
    model.summary()
    model.load_weights('weights.ckpt')
    # modelPredict = ModelPredict(model.encoder, model.decoder, max_pred_len=25, start_token=data.start_token,
    #                             end_token=data.end_token)
    for t in range(700):

        inputs = data.sample(30)
        loss = model(inputs).numpy()

        if t % 500 == 0:
            print(
                "t: ", t,
                "| loss: %.5f" % loss
            )
    model.save_weights('weights.ckpt')
    print('save weights')
    del model
    model = ModelTrain(
        data.num_word, data.num_word, emb_dim=80, units=64, attention_layer_size=20)
    model.build(input_shape=[(None, 20), (None, 20), (None,)])
    model.summary()
    model.load_weights('weights.ckpt')
    print('load weights')
    modelPredict = ModelPredict(model.encoder, model.decoder, max_pred_len=20, start_token=data.start_token,
                                end_token=data.end_token)

    for t in range(50):
        bx, by, decoder_len = data.sample(3)
        target = data.idx2str(by[0, 1:-1])
        # with summary_writer.as_default():
        #     graph = model.inference.get_concrete_function(bx[0:1]).graph
        #     summary_ops_v2.graph(graph.as_graph_def())
        pred = modelPredict(bx[0:1])
        res = data.idx2str(pred[0])
        src = data.idx2str(bx[0])

        print(
            "| input: ", src,
            "| target: ", target,
            "| inference: ", res,
        )


if __name__ == "__main__":
    train()
