from pprint import pprint

import keras.preprocessing.sequence
import numpy as np
import nltk


nltk.download('punkt')

PAD_ID = 0

Max_length = 20


class ConversationData:
    def __init__(self):
        np.random.seed(1)
        self.data_question, self.data_answer, self.words = self.getData()

        self.vocab = set(
            [i for i in self.words] + ["<GO>", "<EOS>"])
        self.v2i = {v: i for i, v in enumerate(sorted(list(self.vocab)), start=1)}
        self.v2i["<PAD>"] = PAD_ID
        self.vocab.add("<PAD>")
        self.i2v = {i: v for v, i in self.v2i.items()}
        self.x, self.y = [], []
        for question, answer in zip(self.data_question, self.data_answer):
            self.x.append([self.v2i[v] for v in nltk.word_tokenize(question)])
            self.y.append(
                [self.v2i["<GO>"], ] + [self.v2i[v] for v in nltk.word_tokenize(answer)] + [
                    self.v2i["<EOS>"]])
        self.x = keras.preprocessing.sequence.pad_sequences(self.x, maxlen=Max_length, padding='post')
        self.y = keras.preprocessing.sequence.pad_sequences(self.y, maxlen=Max_length, padding='post')
        self.x, self.y = np.array(self.x), np.array(self.y)
        self.start_token = self.v2i["<GO>"]
        self.end_token = self.v2i["<EOS>"]

    def sample(self, n=64):
        bi = np.random.randint(0, len(self.x), size=n)
        bx, by = self.x[bi], self.y[bi]
        decoder_len = np.full((len(bx),), by.shape[1] - 1, dtype=np.int32)
        return bx, by, decoder_len

    def analyserText(self, text):
        req = [[self.v2i[v] for v in nltk.word_tokenize(text)]]
        req = keras.preprocessing.sequence.pad_sequences(req, maxlen=Max_length, padding='post')
        return req

    def getData(self):
        f = open(r"H:\software\tensor\data.txt", encoding="utf8")
        sentences = f.readlines()
        f.close()
        x = []
        y = []
        words = []
        q = 1
        for sentence in sentences:
            if q % 2 == 1:
                sentence = sentence.strip('\n')
                x.append(sentence)
                words = words + nltk.word_tokenize(sentence)
                q += 1

            else:

                sentence = sentence.strip('\n')
                y.append(sentence)
                words = words + nltk.word_tokenize(sentence)
                q -= 1

        return x, y, words

    def idx2str(self, idx):
        x = []
        for i in idx:
            if i != PAD_ID and i != self.start_token:
                if i == self.end_token:
                    break
                x.append(self.i2v[i])
                x.append(' ')

        return "".join(x)

    @property
    def num_word(self):
        return len(self.vocab)
