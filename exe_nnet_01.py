import numpy as np
import tensorflow as tf
from keras import layers
from keras import datasets, optimizers
from tensorflow import keras
from matplotlib import pyplot

import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# x[60k,28,28]
# y[60k]
(x, y), (x_test, y_test) = datasets.mnist.load_data()
for i in range(9):
    pyplot.subplot(330 + 1 + i)
    pyplot.imshow(x[i], cmap=pyplot.get_cmap('gray'))
    pyplot.show()

# x[0,255]=>[0,1]
x = tf.convert_to_tensor(x, dtype=tf.float32) / 255.
# uniform
y = tf.convert_to_tensor(y, dtype=tf.int32)

print(x.shape, x.dtype, y.shape, y.dtype)
print(tf.reduce_min(x), tf.reduce_max(x))
# min max
print(tf.reduce_min(y), tf.reduce_max(y))

train_db = tf.data.Dataset.from_tensor_slices((x, y)).batch(128)

train_iter = iter(train_db)
sample = next(train_iter)
print('branch', sample[0].shape, sample[1].shape)
lr = 1e-3
w1 = tf.Variable(tf.random.truncated_normal([784, 256], stddev=0.1))
b1 = tf.Variable(tf.zeros([256]))
w2 = tf.Variable(tf.random.truncated_normal([256, 128], stddev=0.1))
b2 = tf.Variable(tf.zeros([128]))
w3 = tf.Variable(tf.random.truncated_normal([128, 10], stddev=0.1))
b3 = tf.Variable(tf.zeros([10]))

for epoch in range(10):
    for step, (x, y) in enumerate(train_db):
        # x 128,28,28
        # y 128
        x = tf.reshape(x, [-1, 28 * 28])
        with tf.GradientTape() as tape:  # 只跟踪tf.variable
            h1 = x @ w1 + b1
            h1 = tf.nn.relu(h1)
            h2 = h1 @ w2 + b2
            h2 = tf.nn.relu(h2)
            out = h2 @ w3 + b3
            y_one_hot = tf.one_hot(y, depth=10)
            # mse= mean(sum((y_one_hot-out)^2))
            # [b,10]
            loss = tf.square(y_one_hot - out)
            # mean scalar
            loss = tf.reduce_mean(loss)

        # 梯度计算
        grads = tape.gradient(loss, [w1, b1, w2, b2, w3, b3])

        w1.assign_sub(lr * grads[0])
        b1.assign_sub(lr * grads[1])
        w2.assign_sub(lr * grads[2])
        b2.assign_sub(lr * grads[3])
        w3.assign_sub(lr * grads[4])
        b3.assign_sub(lr * grads[5])

        if step % 100 == 0:
            print(step, 'loss :', float(loss))
