import tensorflow as tf


class Adder(tf.Module):
    @tf.function(input_signature=[tf.TensorSpec(shape=[], dtype=tf.float32)])
    def add(self, x):
        return x + x


model = Adder()
tf.saved_model.save(model, '/tmp/adder')
