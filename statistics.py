import numpy as np
import tensorflow as tf
from keras import layers
from keras import datasets, optimizers
from tensorflow import keras

# 第二范数
a = tf.ones([2, 2])
print(tf.norm(a))
# 第一范数
print(tf.norm(a, ord=1))
# 第二范数，按照第二个维度来求解，矩阵看成一列行向量，每个行向量求范数
print(tf.norm(a, ord=2, axis=1))
# 求最大，最小，和均值
b = tf.random.normal([3, 4])
print(tf.reduce_min(b))
print(tf.reduce_min(b, axis=1))
# 求最大，最小，的位置
print(tf.argmax(b))
c = tf.constant([0, 2, 4, 6])
d = tf.range(4)
res = tf.equal(c, d)
print(tf.reduce_sum(tf.cast(res, dtype=tf.int32)))
# Accuracy
pred = tf.constant([[0.3, 0.1, 0.6], [0.02, 0.08, 0.9]])
res = tf.argmax(pred, axis=1)
y = tf.constant([2, 2], dtype=tf.int64)
compare = tf.equal(y, res)
print("right", tf.reduce_sum(tf.cast(compare, dtype=tf.int32)))
# Unique ,Gather
l = tf.constant([4, 3, 3, 2, 4])
u = tf.unique(l)
print(u)
print(tf.gather(u.y, u.idx))
