import tensorflow as tf

tf.random.set_seed(2467)


# out为神经网路的输出结果，b个数据，N个分类概率，target为b个数据的实际分类，topk为一维向量，是topk取值序列
def accuracy(out, target, topk=(1,)):
    maxk = max(topk)
    # batch size 为target数据个数
    batch_size = target.shape[0]

    # 对于每个数据，前maxk个可能性的位置
    pred = tf.math.top_k(out, maxk).indices
    # 转至预言结果，每个maxk取值，第几可能性，都有对应的预测结果
    pred = tf.transpose(pred, perm=[1, 0])
    # 将target扩充
    target_ = tf.broadcast_to(target, pred.shape)
    # 比较预测和目标
    correct = tf.equal(pred, target_)

    res = []
    for k in topk:
        # 前k行，维度变换为一维，并将boolean型转化为float型，前k个可能，最多能命中1个
        correct_k = tf.cast(tf.reshape(correct[:k], [-1]), dtype=tf.float32)
        # 求和
        correct_k = tf.reduce_sum(correct_k)
        # 计算精确度
        acc = float(correct_k / batch_size)
        res.append(acc)
    return res


output = tf.random.normal([10, 6])
# 10行，10个数据，6列，六个分类，按列softmax，每行的和为1
output = tf.math.softmax(output, axis=1)
# 分类，0-5
target = tf.random.uniform([10], maxval=6, dtype=tf.int32)
print('prob:', output.numpy())
# 求分类的排列，axis指向分类所在的维度
pred = tf.argmax(output, axis=1)
print('pred:', pred.numpy())
print('label:', target.numpy())
acc = accuracy(output, target, topk=(1, 2, 3, 4, 5, 6))
print('top1-6 acc:', acc)
