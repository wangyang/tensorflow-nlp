import numpy as np


# 0123代表左下右上，返回移动后的位置，上右下左
def Move(pos, dir):
    new_x = pos[0]
    new_y = pos[1]
    if dir == 0:
        new_x -= 1
    elif dir == 1:
        new_y += 1
    elif dir == 2:
        new_x += 1
    elif dir == 3:
        new_y -= 1
    else:
        print('Direction invalide')

    if (new_x < 0):
        new_x = 0
    elif (new_x > 2):
        new_x = 2
    elif (new_y < 0):
        new_y = 0
    elif (new_y > 2):
        new_y = 2

    return (new_x, new_y)


# La meilleure direction est (l'une de) celle(s) qui
# 找Q值最大的相邻策略
# contien(nen)t la plus grande valeur de Q
# dans le voisinage de la position pos

def bestDirection(tabQ, pos):
    x = pos[0]
    y = pos[1]
    best = []
    mx = max(tabQ[x][y][0], tabQ[x][y][1], tabQ[x][y][2], tabQ[x][y][3])
    # 取所有行动的最大价值
    for i in range(4):
        if (tabQ[x][y][i] == mx):

            best.append(i)
    # 取最大价值，对应位置
    choice = np.random.randint(len(best))
    # 随机选取一个最大收益行动
    print("choice=", choice)
    print("best=", best)
    print("best choice=", best[choice])
    print("best value=", tabQ[x][y][best[choice]])
    return best[choice], tabQ[x][y][best[choice]]


# Valeur du reward correspondant à une case
# 列表Q（位置x，位置y，最佳方向）
# 阿尔法，学习率
# gamma，衰减系数
# start，开始位置
# nbiter，迭代次数
# tauxExplor，一个概率，允许产生非最佳决策
# obj，目标位置
# cobj，惩罚位置

# Coeur de l'algorithme : mise-à-jour de la table Q
# Q(s,a) = Q(s,a) + alpha(r+gamma*maxQ(s',a')-Q(s,a))

def updateQ(tabQ, alpha, gamma, start, nbIter, tauxExplor, obj, cobj):
    for numiter in range(nbIter):
        pos = start
        while ((pos != obj) and (pos != cobj)):
            rnd = np.random.rand()
            if (rnd < tauxExplor):
                dir = np.random.randint(3)
            else:
                dir = bestDirection(tabQ, pos)[0]


            new_x, new_y = Move(pos, dir)

            r = tabReward[new_x][new_y]
            maxQ = max(tabQ[new_x][new_y][0], tabQ[new_x][new_y][1], tabQ[new_x][new_y][2], tabQ[new_x][new_y][3])
            # 更新对应位置的对应策略的奖励
            tabQ[pos[0]][pos[1]][dir] = tabQ[pos[0]][pos[1]][dir] + alpha * (r + gamma*(maxQ - tabQ[pos[0]][pos[1]][dir]))
            pos = (new_x, new_y)


def afficherStratOptim(tabQ):
    for x in range(3):
        for y in range(3):
            print(x, y, np.argmax((tabQ[x][y][0], tabQ[x][y][1], tabQ[x][y][2], tabQ[x][y][3])))
            # 返回最大值所在位置


# Programme principal
# gridWorld = np.zeros(3,3,4)

tabQ = np.zeros([3, 3, 4])
tabReward = np.zeros([3, 3])
tabReward[0][2] = 1
tabReward[1][1] = -1

alpha = 0.2
gamma = 0.9
start = (2, 0)
nbIter = 1000
tauxExplor = 0.5
obj = (0, 2)
cobj = (1, 1)

updateQ(tabQ, alpha, gamma, start, nbIter, tauxExplor, obj, cobj)
# 阿尔法学习率，伽马衰减率
afficherStratOptim(tabQ)
print(tabQ)
